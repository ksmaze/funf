package edu.mit.media.funf.probe.builtin;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import edu.mit.media.funf.Schedule;
import edu.mit.media.funf.math.FFT;
import edu.mit.media.funf.math.MFCC;
import edu.mit.media.funf.math.Window;
import edu.mit.media.funf.probe.Probe;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-5-15
 * Time: 下午5:36
 * To change this template use File | Settings | File Templates.
 */
@Schedule.DefaultSchedule(interval = 60, duration = 15)
@Probe.RequiredFeatures("android.hardware.microphone")
@Probe.RequiredPermissions(android.Manifest.permission.RECORD_AUDIO)
public class VoiceFeatureProbe extends Probe.Base implements Probe.ContinuousProbe, ProbeKeys.VoiceFeatureKeys {

    private static int RECORDER_SOURCE = MediaRecorder.AudioSource.VOICE_RECOGNITION;
    private static int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;
    private static int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private static int RECORDER_SAMPLERATE = 44100;
    private static int MFCCS_VALUE = 12;
    private static int MEL_BANDS = 20;
    short data16bit[];
    private int FFT_SIZE = 8192;
    private Thread recordingThread = null;
    private int bufferSize = 0;
    private int bufferSamples = 0;
    private FFT featureFFT = null;
    private MFCC featureMFCC = null;
    private Window featureWin = null;
    private AudioRecord audioRecorder = null;

    @Override
    protected void onStart() {
        super.onStart();
        if (audioRecorder == null) {
            bufferSize = AudioRecord.getMinBufferSize(
                    RECORDER_SAMPLERATE,
                    RECORDER_CHANNELS,
                    RECORDER_AUDIO_ENCODING);

            //bufferSize = Math.max(bufferSize, RECORDER_SAMPLERATE*2);
            bufferSamples = bufferSize * 4;
            FFT_SIZE = bufferSamples;

            featureFFT = new FFT(FFT_SIZE);
            featureWin = new Window(bufferSamples);
            featureMFCC = new MFCC(FFT_SIZE, MFCCS_VALUE, MEL_BANDS, RECORDER_SAMPLERATE);

            audioRecorder = new AudioRecord(
                    RECORDER_SOURCE,
                    RECORDER_SAMPLERATE,
                    RECORDER_CHANNELS,
                    RECORDER_AUDIO_ENCODING,
                    bufferSize);
            audioRecorder.startRecording();
            data16bit = new short[bufferSamples];
        }

        final Runnable r = new Runnable() {
            @Override
            public void run() {
                handleAudioStream();
                getHandler().postDelayed(this, 2000);
            }
        };
        getHandler().postDelayed(r, 2000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        audioRecorder.stop();
        audioRecorder.release();
        audioRecorder = null;
        recordingThread = null;
    }

    private void handleAudioStream() {
        double fftBufferR[] = new double[FFT_SIZE];
        double fftBufferI[] = new double[FFT_SIZE];
        List<Double> featureCepstrum;
        int readAudioSamples = 0;
        if (State.RUNNING.equals(getState())) {
            readAudioSamples = audioRecorder.read(data16bit, 0, bufferSamples);
            JsonObject data = new JsonObject();
            if (readAudioSamples > 0) {
                double fN = (double) readAudioSamples;

                // L1-norm
                double accum = 0;
                for (int i = 0; i < readAudioSamples; i++) {
                    accum += Math.abs((double) data16bit[i]);
                }

                data.addProperty(DECIBEL, 10.0F * Math.log(accum / fN * 2));
                // Frequency analysis
                Arrays.fill(fftBufferR, 0);
                Arrays.fill(fftBufferI, 0);

                // Convert audio buffer to doubles
                for (int i = 0; i < readAudioSamples; i++) {
                    fftBufferR[i] = data16bit[i];
                }

                // In-place windowing
                featureWin.applyWindow(fftBufferR);

                // In-place FFT
                featureFFT.fft(fftBufferR, fftBufferI);

                Gson gson = getGson();

                featureCepstrum = featureMFCC.cepstrumArray(fftBufferR, fftBufferI);


                int speak = 0;
                try {
                    speak = (int) WekaClassifier.classify(featureCepstrum.toArray());
                    Log.e("Funf", "conversation :" + speak);
                    //
                } catch (Exception e) {
                    e.printStackTrace();
                }
                data.addProperty(CONVERSATION, speak);

                data.add(MFCCS, gson.toJsonTree(featureCepstrum));

                // Write out features
                sendData(data);

                //Log.e("Funf", Thread.currentThread().toString());

            }
        }

    }

    static class WekaClassifier {

        public static double classify(Object[] i)
                throws Exception {

            double p = Double.NaN;
            p = WekaClassifier.N4c6932cb0(i);
            return p;
        }
        static double N4c6932cb0(Object []i) {
            double p = Double.NaN;
            if (i[11] == null) {
                p = 0;
            } else if (((Double) i[11]).doubleValue() <= 2.1957395951976624) {
                p = WekaClassifier.Ne85d49e1(i);
            } else if (((Double) i[11]).doubleValue() > 2.1957395951976624) {
                p = WekaClassifier.N3ceda73d6(i);
            }
            return p;
        }
        static double Ne85d49e1(Object []i) {
            double p = Double.NaN;
            if (i[4] == null) {
                p = 0;
            } else if (((Double) i[4]).doubleValue() <= 3.414172378071447) {
                p = 0;
            } else if (((Double) i[4]).doubleValue() > 3.414172378071447) {
                p = WekaClassifier.N7449d8842(i);
            }
            return p;
        }
        static double N7449d8842(Object []i) {
            double p = Double.NaN;
            if (i[11] == null) {
                p = 0;
            } else if (((Double) i[11]).doubleValue() <= -1.5446839231600697) {
                p = 0;
            } else if (((Double) i[11]).doubleValue() > -1.5446839231600697) {
                p = WekaClassifier.N1c25fdc13(i);
            }
            return p;
        }
        static double N1c25fdc13(Object []i) {
            double p = Double.NaN;
            if (i[0] == null) {
                p = 1;
            } else if (((Double) i[0]).doubleValue() <= 96.484255706171) {
                p = WekaClassifier.N23698574(i);
            } else if (((Double) i[0]).doubleValue() > 96.484255706171) {
                p = 0;
            }
            return p;
        }
        static double N23698574(Object []i) {
            double p = Double.NaN;
            if (i[0] == null) {
                p = 0;
            } else if (((Double) i[0]).doubleValue() <= 83.79917299560911) {
                p = WekaClassifier.N4e4b78e35(i);
            } else if (((Double) i[0]).doubleValue() > 83.79917299560911) {
                p = 1;
            }
            return p;
        }
        static double N4e4b78e35(Object []i) {
            double p = Double.NaN;
            if (i[11] == null) {
                p = 0;
            } else if (((Double) i[11]).doubleValue() <= 0.44876873793722705) {
                p = 0;
            } else if (((Double) i[11]).doubleValue() > 0.44876873793722705) {
                p = 1;
            }
            return p;
        }
        static double N3ceda73d6(Object []i) {
            double p = Double.NaN;
            if (i[4] == null) {
                p = 0;
            } else if (((Double) i[4]).doubleValue() <= 1.0726302203160671) {
                p = WekaClassifier.N167eb50b7(i);
            } else if (((Double) i[4]).doubleValue() > 1.0726302203160671) {
                p = 1;
            }
            return p;
        }
        static double N167eb50b7(Object []i) {
            double p = Double.NaN;
            if (i[11] == null) {
                p = 0;
            } else if (((Double) i[11]).doubleValue() <= 3.1256365786392903) {
                p = 0;
            } else if (((Double) i[11]).doubleValue() > 3.1256365786392903) {
                p = 1;
            }
            return p;
        }
    }



}
